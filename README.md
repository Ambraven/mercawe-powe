Mershawe-powe
=============

Here are some tools to ease designing a conlang originated from [Witches.town](https://witches.town) called Mershawe.

Not much to see yet, not a standalone program but some modules you can load into an interpreter to play with

```bash
$ ghci Vocabulary
GHCi, version 8.0.1: http://www.haskell.org/ghc/  :? for help
[1 of 3] Compiling Toolbox          ( Toolbox.hs, interpreted )
[2 of 3] Compiling Phonology        ( Phonology.hs, interpreted )
[3 of 3] Compiling Vocabulary       ( Vocabulary.hs, interpreted )
Ok, modules loaded: Vocabulary, Phonology, Toolbox.
*Vocabulary> generate
kae
*Vocabulary> generate
ye
*Vocabulary> generate
biya
*Vocabulary> generate
ay
```
