module Phonology (
    Consomnant(..),
    consomnants,
    Sound(..),
    sounds,
    syllable,
    Vowel(..),
    vowels
  ) where

import Data.Char (toLower)
import Toolbox (oneOf)
import System.Random (randomIO)

data Consomnant =
    K
  | G
  | S
  | Z
  | J
  | C
  | D
  | T
  | N
  | H
  | B
  | P
  | M
  | Y
  | R
  | W
  deriving (Show, Ord, Eq, Enum)

data Vowel =
    A
  | E
  | I
  | O
  | U
  deriving (Show, Ord, Eq, Enum)

data Sound = Consomnant Consomnant | Vowel Vowel deriving (Ord, Eq)

instance Show Sound where
  show (Consomnant c) = map toLower (show c)
  show (Vowel v) = map toLower (show v)

consomnants :: [ Sound ]
consomnants = map Consomnant [ K .. W ]

vowels :: [ Sound ]
vowels = map Vowel [ A .. U ]

sounds :: [ Sound ]
sounds = consomnants ++ vowels

syllable :: IO [ Sound ]
syllable = do
  s <- oneOf sounds
  l <- if s `elem` vowels then return [] else fmap (:[]) $ oneOf vowels
  endSyllable $ s : l
  where
    endSyllable syllable = do
      addSound <- randomIO
      if addSound
        then fmap (\s -> syllable ++ [s]) $ oneOf sounds
        else return syllable
