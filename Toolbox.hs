module Toolbox (
    oneOf,
    weighted
  ) where

import System.Random

oneOf :: [ a ] -> IO a
oneOf l = do
  i <- randomRIO (0, length l - 1)
  return $ l !! i

weighted :: [ (a, Float) ] -> IO a
weighted l = do
  let total = sum $ fmap snd l
  i <- randomRIO (0, total - 1)
  return $ get i l
  where
    get _ [(x, _)] = x
    get k ((x, w) : l) = if k < w then x else get (k - w) l
