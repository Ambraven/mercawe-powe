module Vocabulary (
    generate
  ) where

import Phonology
import Data.Char (toLower)
import Data.List (intercalate)
import Control.Monad (forM)
import Toolbox (weighted)

data Word = Word [ Sound ] deriving (Ord, Eq)

instance Show Vocabulary.Word where
  show (Word l) = intercalate "" $ map show l

generate :: IO Vocabulary.Word
generate = do
  length <- weighted lengths
  fmap (Word . concat) $ forM [1..length] (const syllable)
  where
    lengths = zip [1..3] [7, 5, 2]
